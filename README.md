# Meeting Doctors SDK

A plugin that allows Flutter apps interact with Meeting Doctors native SDKs from Android and iOS. 

## Installation
### Add plugin dependency
Add this package to your package's `pubspec.yaml`.
While plugin is not published, use plugin locally, adding this to pubspec:
```dart
meeting_doctors_sdk:
    # When depending on this package from a real application you should use:
    #   meeting_doctors_sdk: ^x.y.z
    # See https://dart.dev/tools/pub/dependencies#version-constraints
    # The example app is bundled with the plugin so we use a path dependency on
    # the parent directory to use the current plugin's version.
    path: ../
```
### Add firebase to the project
To use this plugin, it is required to setup Firebase in your project.
In case of the example provided inside the plugin package, demo app will not start if firebase is not setup correctly.

##### Android
Actually, plugin does not support Android, in the future it will be added.

##### iOS
In iOS there is no need to include dependency in native side, because firebase dependencies can be added in Flutter, but it is required to configure APNs certificates and include the `GoogleService-Info.plist`.
The configuration process is exactly the same as in other iOS projects:
[APNs certificates configuration](https://firebase.google.com/docs/cloud-messaging/ios/certs)
[Add firebase configuration](https://firebase.google.com/docs/cloud-messaging/ios/client)

##### Flutter
It is required to add firebase messanging and firebase core dependency in Flutter project:
[Firebase Core](https://pub.dev/packages/firebase_core)
[Firebase Messaging](https://pub.dev/packages/firebase_messaging)

For instance, add these line to `pubspec.yaml`:
```dart
firebase_core: ^0.7.0
firebase_messaging: ^8.0.0-dev.14
```
 Keep in mind that dependencies are updated very often, sometimes with breaking changes, so we strongly recommend update firebase dependencies from time to time.
To be able to run app successfully, you must initialize firebase app as soon as possible:
```dart
 void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
```

## Access permissions
Access to camera, photo gallery and microphone always requires explicit permission from the user.
Your app must provide an explanation.
Following key should be added to `Info.plist`:
```
NSCameraUsageDescription
NSPhotoLibraryUsageDescription
NSMicrophoneUsageDescription
```
iOS displays this explanation when initially asking the user for permission to attach an element in the current conversation.
⚠️ Attempting to attach a gallery photo or start a camera session without an usage description will raise an exception.

## Usage

### Import the libraries
Import package with:
```dart
import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';
```
#### Meeting Doctors SDK platform channel
##### Initialize
First of all, it is required initialize SDK.
There are 2 options:
- Put the initialization code in `AppDelegate.swift`
In this case, you can read the section 'Integration' in [native iOS guide](https://bitbucket.org/meetingdoctors-team/ios-sdk-sample/src/master/#Integration)
Using this approach, you can use full capabilities from native SDK.

- Use initialize method from Flutter Plugin (WORK IN PROGRESS)
In this case, we have a created a method to initialize SDK from Flutter, but by the moment, only api key and client secret can be provided from Flutter. In the future, all the other parameters should be added.
```dart
Future<InitializeResult> initialize({
String clientName,
String clientSecret,
});
```
```dart
// Initialize Meeeting Doctors SDK
//
// Parameters:
// [clientName] Client name that you should provide
// [clientSecret] Api key provided by Meeting Doctors
//
// Returns InitializeResult with properties:
// [success]: true if initialization has been successful, false otherwise
// [uuid]: Installation identifier
// [platformException]: PlatformException from native platform in case of error
```
##### Authentication
Authentication verifies that the provided user token is correct and, therefore, it can initiate the chat.
```dart
Future<AuthenticateResult> authenticate({String userToken});
```
```dart
// Authenticate with a user token
//
// Parameters:
// [userToken] User token that you should provide
//
// Returns AuthenticateResult with properties:
// [success]: true if platform method has been executed successfully
// [isAuthenticated]: True if authentication is successful, false otherwise
// [platformException]: PlatformException from native platform in case of error
```

##### isAuthenticated
When user authenticates, Meeting doctors stores if user is autenticathed or not
```dart
Future<IsAuthenticatedResult> isAuthenticated();
```
```dart
// To check if a user is authenticated or not
//
// Returns IsAuthenticatedResult with properties:
// [success]: true if platform method has been executed successfully
// [isAuthenticated]: True if authentication is successful, false otherwise
// [platformException]: PlatformException from native platform in case of error
```

##### Send Firebase Cloud Messaging Token
After authentication, it is required to send the token to meeting doctors SDK, otherwise push notifications will not be received.
```dart
  Future<SendFcmTokenResult> sendFcmToken({String fcmToken})
```
```dart
// Send firebase token to meeting doctors sdk to register it
//
// Parameters:
// [fcmToken] Firebase token that should be provided from Flutter side
//
// Returns SendFcmTokenResult with properties:
// [success]: true if platform method has been executed successfully, so token is registered successfully
// [platformException]: PlatformException from native platform in case of error
```

##### Open Chat
Once user has been authenticated, native chat view can be open.
```
Future<OpenChatResult> openChat();
```
```dart
// Open chat native view.
// In case of iOS, it will launch a view controller
// In case of Android, it will launch Activity or Fragment
//
// Returns OpenChatResult with properties:
// [success]: true if platform method has been executed successfully, so chat has been opened
// [platformException]: PlatformException from native platform in case of error
```

##### Video call
Once user has been authenticated, if video call is enabled, native video call view can be open
```dart
Future<OpenVideoCallResult> openVideoCall();
```
```dart
// Open video call native view.
// In case of iOS, it will launch a view controller
// In case of Android, it will launch Activity or Fragment
//
// Returns OpenChatResult with properties:
// [success]: true if platform method has been executed successfully, so video call has been open
// [platformException]: PlatformException from native platform in case of error
```

##### Logout
This method should be called only if user is authenticated.
```dart
Future<LogoutResult> logout();
```
```dart
// To finish the user session.
// Logout method delete all user cached data, including chat messages and professional list
//
// Returns LogoutResult with properties:
// [success]: true if platform method has been executed successfully, so logout has been successful
// [platformException]: PlatformException from native platform in case of error
```

##### Shutdown
A shutdown method exists so a chat user can disconnect and erase all sensible information.
```dart
Future<ShutdownResult> shutdown();
```
```dart
// A shutdown method exists so a chat user can disconnect and erase all sensible information:
//
// Returns ShutdownResult with properties:
// [success]: true if platform method has been executed successfully, so shutdown has been successful
// [platformException]: PlatformException from native platform in case of error
```

## Demo app
We have created a sample app only with demo purposes.
To run app successfully, some configuration is needed.

##### Firebase configuration
In order to enable MeetingDoctors Chat notifications, there are two posibilities: 1.- If you have your own Firebase app declared, you must provide us Sender ID and Server Key from your Firebase workspace. 2.- If you don't have a Firebase and don't want to create it, we can provide one. For Android we need your App Package, and for iOS we need .p8 Certificate file, Team ID, and the certificate key. Once Firebase app are created, we'll provide you google-services.json and GoogleService-info.plist files to add to your apps.
If app is not open, probably `Firebase.initializeApp()` is causing the issue:
```dart
 void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
```
##### Configure Api key, client name and user token
Before launching the app, you must review the configuration in `\example\lib\di\DI.dart`:
```dart
class DI {
  ....

  static final String apiKey = "YOUR API KEY";
  static final String clientName = "YOUR CLIENT NAME";
  static final String userToken = "YOUR USER TOKEN";
}
```
Just update the these variables with values provided by Meeting Doctors.
##### Demo app architecture overview
In this demo app, we don't use any package for state management. We try to keep things as simple as possible.
We try to encapsulate logic behind some interfaces, that can be accessed using the class `DI.dart`:
```dart
class DI {
  static final PushNotificationsManager pushManager = PushNotificationsManager(FirebaseMessaging.instance);
  static final PermissionsRepository permissionsRepository = PermissionsRepositoryImpl();
  static final MeetingDoctorsRepository meetingDoctorsRepository = MeetingDoctorsRepositoryImpl(MeetingDoctorsSdk(), pushManager);
  .....
}
```
In this sample app, we have used some kind of repository pattern, to separate plugin platform interface from UI.
In a production ready app, probably it will be needed use some kind of state management solution.
Here we have a sample usage of the meeting doctors sdk plugin.
For instance, when we press on 'Login' button, we authenticate with user and send firebase token to Meeting Doctors SDK:
```dart
RaisedButton(
   child: const Text('Login'),
    onPressed: () async {
        if (_formKey.currentState.validate()) {
            AuthenticateResult authenticateResult =
            await DI.meetingDoctorsRepository.authenticate(userToken: _userToken);
            SendFcmTokenResult sendFcmTokenResult = await DI.meetingDoctorsRepository.sendFcmToken();
            setState(() {
                _isAuthenticated = authenticateResult.isAuthenticated;
                 _isTokenRegistered = sendFcmTokenResult.success;
            });
        }
    },
)
```
Once we receive data, we update UI using `setState`.

##### Access Permissions
In Flutter, request permissions is straight forward using the following package:
[Permission handler](https://pub.dev/packages/permission_handler)
```dart
@override
  Future<bool> requestCameraPermissions() async {
    PermissionStatus permissionStatus = await Permission.camera.request();
    return permissionStatus == PermissionStatus.granted;
  }
```


## Next Steps
- Add more functionalities to the plugin
- Improve plugin based in the feedback received
- Implement Android native plugin