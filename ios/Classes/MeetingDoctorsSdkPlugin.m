#import "MeetingDoctorsSdkPlugin.h"
#if __has_include(<meeting_doctors_sdk/meeting_doctors_sdk-Swift.h>)
#import <meeting_doctors_sdk/meeting_doctors_sdk-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "meeting_doctors_sdk-Swift.h"
#endif

@implementation MeetingDoctorsSdkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMeetingDoctorsSdkPlugin registerWithRegistrar:registrar];
}
@end
