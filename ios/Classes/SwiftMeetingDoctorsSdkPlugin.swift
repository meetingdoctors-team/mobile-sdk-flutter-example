import Flutter
import UIKit
import MediQuo

public class SwiftMeetingDoctorsSdkPlugin: NSObject, FlutterPlugin, FlutterApplicationLifeCycleDelegate, UNUserNotificationCenterDelegate {
    var launchOptions: [UIApplication.LaunchOptionsKey : Any]?
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "meeting_doctors_sdk", binaryMessenger: registrar.messenger())
        let instance = SwiftMeetingDoctorsSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
        registrar.addApplicationDelegate(instance)
        UNUserNotificationCenter.current().delegate = instance;
    }
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable : Any] = [:]) -> Bool {
        var options = [UIApplication.LaunchOptionsKey: Any]()
        for (k, value) in launchOptions {
            let key = k as! UIApplication.LaunchOptionsKey
            options[key] = value
        }
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Set launchOptions")
        self.launchOptions = options
        return true
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "initialize":
            self.initialize(call, result)
        case "authenticate":
            self.authenticate(call, result)
        case "isAuthenticated":
            self.isAuthenticated(call, result)
        case "openChat":
            self.openChat(call, result)
        case "sendFcmToken":
            self.sendFcmToken(call, result)
        case "logout":
            self.logout(call, result)
        case "shutdown":
            self.shutdown(call, result)
        case "openVideoCall":
            self.openVideoCall(call, result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func initialize(_ call: FlutterMethodCall,_ result: FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, String> else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing arguments",
                    details: nil
                )
            )
            return
        }
        guard let clientName = args["clientName"] else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing client name",
                    details: nil
                )
            )
            return
        }
        guard let clientSecret = args["clientSecret"] else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing client secret",
                    details: nil
                )
            )
            return
        }
        
        let configuration = MediQuo.Configuration(id: clientName, secret: clientSecret, enableVideoCall: true, isDemo: true)
        let uuid: UUID? = MediQuo.initialize(with: configuration, options: launchOptions) {  resultInit in
            guard let value = resultInit.value else {
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Installation failed: '\(String(describing: resultInit.error))'")
                //TODO ADD Flutter Error
                return;
            }
            NSLog("[SwiftMeetingDoctorsSdkPlugin] Mediquo framework initialization succeeded with identifier: '\(value.installationId)'")
        }
        let uuidAsString = uuid?.uuidString ?? "no uuid"
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Synchronous installation identifier: '\(uuidAsString)'")
        //TODO Review why some times the UUID is not received in a synchronous way and is returned like 'no uuid'
        result(uuidAsString)
    }
    
    private func authenticate(_ call: FlutterMethodCall,_ result: @escaping FlutterResult) {
        guard let args = call.arguments as? Dictionary<String, String> else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing arguments",
                    details: nil
                )
            )
            return
        }
        guard let userToken = args["userToken"] else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing user token",
                    details: nil
                )
            )
            return
        }
        MediQuo.authenticate(token: userToken) {
            let success = $0.isSuccess
            if success {
                result(NSNumber(value: true))
            } else {
                result(NSNumber(value: false))
            }
        }
    }
    
    private func isAuthenticated(_ call: FlutterMethodCall,_ result: @escaping FlutterResult) {
        result(NSNumber(value: MediQuo.isAuthenticated))
    }
    
    private func openChat(_ call: FlutterMethodCall,_ result: @escaping FlutterResult) {
        self.openChatViewController { (openChatSuccess) in
            if (openChatSuccess) {
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Open chat success")
            } else {
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Open chat failed")
            }
            result(NSNumber(value: openChatSuccess))
        }
    }
    
    private func sendFcmToken(_ call: FlutterMethodCall,_ result: @escaping FlutterResult){
        guard let args = call.arguments as? Dictionary<String, String> else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing arguments",
                    details: nil
                )
            )
            return
        }
        guard let fcmToken = args["fcmToken"] else {
            result(
                FlutterError(
                    code: "invalidArgs",
                    message: "Missing firebase token",
                    details: nil
                )
            )
            return
        }
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Firebase registration token: \(fcmToken)")
        MediQuo.registerFirebaseForNotifications(token: fcmToken) { resultRegisterToken in
            resultRegisterToken.process(doSuccess: { _ in
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Token registered correctly")
                result(nil)
            }, doFailure: { error in
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Error registering token: \(error)")
                result(
                    FlutterError(
                        code: "sendFcmTokenFailed",
                        message: "Error registering token",
                        details: nil
                    )
                )
            })
        }
    }
    
    private func logout(_ call: FlutterMethodCall,_ result: @escaping FlutterResult){
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Logout")
        MediQuo.logout { (resultLogout: MediQuoResult<Void>) in
            switch resultLogout {
            case .success:
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Logout success")
                result(nil)
            case .failure(let error):
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Logout error \(error)")
                result(
                    FlutterError(
                        code: "logoutError",
                        message: "Error during logout \(error.localizedDescription)",
                        details: nil
                    )
                )
            default:
                result(NSNumber(value: false))
            }
        }
    }
    
    private func shutdown(_ call: FlutterMethodCall,_ result: @escaping FlutterResult){
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Shutdown")
        MediQuo.shutdown { (resultLogout: MediQuoResult<Void>) in
            switch resultLogout {
            case .success:
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Shutdown success")
                result(nil)
            case .failure(let error):
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Shutdown error \(error)")
                result(
                    FlutterError(
                        code: "logoutError",
                        message: "Error during logout \(error.localizedDescription)",
                        details: nil
                    )
                )
            @unknown default:
                result(
                    FlutterError(
                        code: "logoutError",
                        message: "Error during logout: unknown default",
                        details: nil
                    )
                )
            }
        }
    }
    
    private func openVideoCall(_ call: FlutterMethodCall,_ result: @escaping FlutterResult){
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Videocall")
        MediQuo.deeplink(.videoCall, origin: self.topViewController(), animated: true) { resultOpenVideoCall in
            resultOpenVideoCall.process(doSuccess: { _ in
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Open videocall success")
                result(nil)
            }, doFailure: { error in
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Open videocall error \(error)")
                result(
                    FlutterError(
                        code: "openVideoCallError",
                        message: "Error during open video call \(error.localizedDescription)",
                        details: nil
                    )
                )
            })
        }
    }
    
    public func userNotificationCenter(_ userNotificationCenter: UNUserNotificationCenter,
                                       willPresent notification: UNNotification,
                                       withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        MediQuo.userNotificationCenter(userNotificationCenter, willPresent: notification) { result in
            do {
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Case 2")
                completionHandler(try result.unwrap())
            } catch {
                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    
    public func userNotificationCenter(_ userNotificationCenter: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse,
                                       withCompletionHandler completionHandler: @escaping () -> Void) {
        MediQuo.userNotificationCenter(userNotificationCenter, didReceive: response) { result in
            NSLog("[SwiftMeetingDoctorsSdkPlugin] Case 3")
            result.process(doSuccess: { _ in
                completionHandler()
            }, doFailure: { error in
                if let mediQuoError = error as? MediQuoError,
                   case let .messenger(reason) = mediQuoError,
                   case let .cantNavigateTopViewControllerIsNotMessengerViewController(deeplinkOption) = reason {
                    // put chat view controller on top of navigation and launch the deeplink
                    self.openChatViewController({ (openChatSuccess) in
                        if (openChatSuccess) {
                            MediQuo.deeplink(.messenger(option: deeplinkOption), origin: self.topViewController(), animated: true) { result in
                                result.process(doSuccess: { _ in
                                    NSLog("[SwiftMeetingDoctorsSdkPlugin] Chat navigation success")
                                }, doFailure: { error in
                                    NSLog("[SwiftMeetingDoctorsSdkPlugin] Can't navigate chat deeplink: \(error)")
                                })
                            }
                        } else {
                            NSLog("[SwiftMeetingDoctorsSdkPlugin] Can't navigate because can't open chat")
                        }
                        completionHandler()
                    })
                    
                } else if let mediQuoError = error as? MediQuoError,
                          case let .videoCall(reason) = mediQuoError,
                          case .cantNavigateExternalOriginIsRequired = reason {
                    // put chat view controller on top of navigation and launch the deeplink
                    MediQuo.deeplink(.videoCall, origin: self.topViewController(), animated: true) { result in
                        result.process(doSuccess: { _ in
                            NSLog("[SwiftMeetingDoctorsSdkPlugin] Videocall navigation succeess")
                        }, doFailure: { error in
                            NSLog("[SwiftMeetingDoctorsSdkPlugin] Can't navigate videocall deeplink: \(error)")
                        })
                    }
                    completionHandler()
                } else {
                    NSLog("[SwiftMeetingDoctorsSdkPlugin] Error user notification center: \(error)")
                    completionHandler()
                }
            })
        }
    }
    
    private func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let tab = base as? UITabBarController,
           let selected = tab.selectedViewController {
            return self.topViewController(selected)
        }
        if let nav = base as? UINavigationController,
           let visible = nav.visibleViewController {
            return self.topViewController(visible)
        }
        if let presented = base?.presentedViewController {
            return self.topViewController(presented)
        }
        return base
    }
    
    private func openChatViewController(_ completion: @escaping ((Bool) -> ())){
        if let uiViewController = UIApplication.shared.keyWindow?.rootViewController {
            let messengerResult = MediQuo.messengerViewController()
            if let chatViewController: UINavigationController = messengerResult.value {
                uiViewController.present(chatViewController, animated: true, completion: {
                    completion(true)
                })
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
}
