import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';
import 'package:meeting_doctors_sdk_example/di/DI.dart';
import 'package:meeting_doctors_sdk_example/push/PushNotificationManager.dart';
import 'package:meeting_doctors_sdk_example/repository/Repository.dart';

class MeetingDoctorsRepositoryImpl extends MeetingDoctorsRepository {
  final PushNotificationsManager pushNotificationsManager;
  final MeetingDoctorsSdk meetingDoctorsSdk;

  MeetingDoctorsRepositoryImpl(this.meetingDoctorsSdk, this.pushNotificationsManager);

  @override
  Future<InitializeResult> initialize({clientName: String}) {
    return meetingDoctorsSdk.initialize(
      clientName: clientName,
      clientSecret: DI.apiKey,
    );
  }

  @override
  Future<AuthenticateResult> authenticate({userToken: String}) {
    return meetingDoctorsSdk.authenticate(userToken: userToken);
  }

  @override
  Future<IsAuthenticatedResult> isAuthenticated() {
    return meetingDoctorsSdk.isAuthenticated();
  }

  @override
  Future<SendFcmTokenResult> sendFcmToken() async {
    String fcmToken = await pushNotificationsManager.getFcmToken();
    return meetingDoctorsSdk.sendFcmToken(fcmToken: fcmToken);
  }

  @override
  Future<OpenChatResult> openChat() {
    return meetingDoctorsSdk.openChat();
  }

  @override
  Future<OpenVideoCallResult> openVideoCall() {
    return meetingDoctorsSdk.openVideoCall();
  }

  @override
  Future<LogoutResult> logout() {
    return meetingDoctorsSdk.logout();
  }

  @override
  Future<ShutdownResult> shutdown() {
    return meetingDoctorsSdk.shutdown();
  }
}
