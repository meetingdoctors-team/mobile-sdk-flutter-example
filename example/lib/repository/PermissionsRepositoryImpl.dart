import 'dart:io';

import 'package:meeting_doctors_sdk_example/repository/Repository.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionsRepositoryImpl extends PermissionsRepository {
  @override
  Future<bool> requestNotificationPermissions() async {
    if (Platform.isAndroid) {
      return true;
    }
    PermissionStatus permissionStatus = await Permission.notification.request();
    return permissionStatus == PermissionStatus.granted;
  }

  @override
  Future<bool> requestCameraPermissions() async {
    PermissionStatus permissionStatus = await Permission.camera.request();
    return permissionStatus == PermissionStatus.granted;
  }

  @override
  Future<bool> requestGalleryPermissions() async {
    Permission galleryPermission = Platform.isIOS ? Permission.photos : Permission.storage;
    PermissionStatus permissionStatus  = await galleryPermission.request();
    return permissionStatus == PermissionStatus.granted;
  }

  @override
  Future<bool> requestMicrophonePermissions() async {
    PermissionStatus permissionStatus = await Permission.microphone.request();
    return permissionStatus == PermissionStatus.granted;
  }
}
