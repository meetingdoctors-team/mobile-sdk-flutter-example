import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';

abstract class MeetingDoctorsRepository {
  Future<InitializeResult> initialize({clientName: String});

  Future<AuthenticateResult> authenticate({userToken: String});

  Future<IsAuthenticatedResult> isAuthenticated();

  Future<SendFcmTokenResult> sendFcmToken();

  Future<OpenChatResult> openChat();

  Future<OpenVideoCallResult>openVideoCall();

  Future<LogoutResult> logout();

  Future<ShutdownResult> shutdown();
}

abstract class PermissionsRepository {
  Future<bool> requestNotificationPermissions();

  Future<bool> requestCameraPermissions();

  Future<bool> requestGalleryPermissions();

  Future<bool> requestMicrophonePermissions();
}
