import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';
import 'package:meeting_doctors_sdk_example/push/PushNotificationManager.dart';
import 'package:meeting_doctors_sdk_example/repository/MeetingDoctorsRepositoryImpl.dart';
import 'package:meeting_doctors_sdk_example/repository/PermissionsRepositoryImpl.dart';
import 'package:meeting_doctors_sdk_example/repository/Repository.dart';

class DI {
  static final PushNotificationsManager pushManager = PushNotificationsManager(FirebaseMessaging.instance);
  static final PermissionsRepository permissionsRepository = PermissionsRepositoryImpl();
  static final MeetingDoctorsRepository meetingDoctorsRepository = MeetingDoctorsRepositoryImpl(MeetingDoctorsSdk(), pushManager);

  static final String apiKey = "YOUR API KEY";
  static final String clientName = "YOUR CLIENT NAME";
  static final String userToken = "YOUR USER TOKEN";
}
