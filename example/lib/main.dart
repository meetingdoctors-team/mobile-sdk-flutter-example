import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:meeting_doctors_sdk_example/styles/styles.dart';
import 'package:meeting_doctors_sdk_example/view/screen/home/HomeScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Meeting Doctors Sdk Demo',
      theme: ThemeData(
        primaryColor: Styles.colorPrimary,
      ),
      home: HomeScreen(),
    );
  }
}
