import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meeting_doctors_sdk_example/di/DI.dart';

class PermissionsCard extends StatefulWidget {
  const PermissionsCard({Key key}) : super(key: key);

  @override
  _PermissionsCardState createState() => _PermissionsCardState();
}

class _PermissionsCardState extends State<PermissionsCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.warning),
              title: const Text('Permissions'),
            ),
            Text(
              'Enable platform permissions before using Meeting Doctors SDK',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
            SizedBox(height: 10),
            RequestPermissionWidget(
              buttonTitle: "Notifications",
              onPressed: () async {
                return DI.permissionsRepository.requestNotificationPermissions();
              },
            ),
            RequestPermissionWidget(
              buttonTitle: "Camera",
              onPressed: () async {
                return DI.permissionsRepository.requestCameraPermissions();
              },
            ),
            RequestPermissionWidget(
              buttonTitle: "Gallery",
              onPressed: () async {
                return DI.permissionsRepository.requestGalleryPermissions();
              },
            ),
            RequestPermissionWidget(
              buttonTitle: "Microphone",
              onPressed: () async {
                return DI.permissionsRepository.requestMicrophonePermissions();
              },
            ),
          ],
        ),
      ),
    );
  }
}

class RequestPermissionWidget extends StatefulWidget {
  final Future<bool> Function() onPressed;
  final String buttonTitle;

  RequestPermissionWidget({
    Key key,
    this.buttonTitle,
    this.onPressed,
  }) : super(key: key);

  @override
  _RequestPermissionWidgetState createState() => _RequestPermissionWidgetState();
}

class _RequestPermissionWidgetState extends State<RequestPermissionWidget> {
  bool _operationSuccess = false;

  static const double COLUMN_PADDING = 16.0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _operationSuccess
            ? Icon(Icons.check_circle_outline, color: Colors.green)
            : Icon(Icons.close_outlined, color: Colors.red),
        SizedBox(width: COLUMN_PADDING),
        RaisedButton(
            onPressed: () async {
              bool success = await widget.onPressed();
              setState(() {
                _operationSuccess = success;
              });
            },
            child: Text(this.widget.buttonTitle)),
      ],
    );
  }
}
