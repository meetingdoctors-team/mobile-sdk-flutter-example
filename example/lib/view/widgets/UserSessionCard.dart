import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';
import 'package:meeting_doctors_sdk_example/di/DI.dart';

class UserSessionCard extends StatefulWidget {
  const UserSessionCard({Key key}) : super(key: key);

  @override
  _UserSessionCardState createState() => _UserSessionCardState();
}

class _UserSessionCardState extends State<UserSessionCard> {
  String _userToken = DI.userToken;

  final _formKey = GlobalKey<FormState>();

  bool _isAuthenticated = false;
  bool _isTokenRegistered = false;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.person),
                title: const Text('User session'),
              ),
              Text(
                'Introduce your user token',
                style: TextStyle(color: Colors.black.withOpacity(0.6)),
              ),
              buildFormInputFields(),
              SizedBox(height: 10),
              _isAuthenticated ? Text("Authenticated") : Text("Not authenticated"),
              SizedBox(height: 10),
              _isTokenRegistered ? Text("Firebase token registered") : Text("Firebase token not registered"),
              ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    child: const Text('Login'),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        AuthenticateResult authenticateResult = await DI.meetingDoctorsRepository.authenticate(userToken: _userToken);
                        SendFcmTokenResult sendFcmTokenResult = await DI.meetingDoctorsRepository.sendFcmToken();
                        setState(() {
                          _isAuthenticated = authenticateResult.isAuthenticated;
                          _isTokenRegistered = sendFcmTokenResult.success;
                        });
                      }
                    },
                  ),
                  RaisedButton(
                    child: const Text('Logout'),
                    onPressed: () async {
                      await DI.meetingDoctorsRepository.logout();
                      IsAuthenticatedResult isAuthenticatedResult = await DI.meetingDoctorsRepository.isAuthenticated();
                      setState(() {
                        _isAuthenticated = isAuthenticatedResult.isAuthenticated;
                        _isTokenRegistered = isAuthenticatedResult.isAuthenticated;
                      });
                    },
                  ),
                ],
              ),
              ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton(
                    child: Text("Open chat".toUpperCase()),
                    onPressed: () async {
                      DI.meetingDoctorsRepository.openChat();
                    },
                  ),
                  FlatButton(
                    child: Text("Open video call".toUpperCase()),
                    onPressed: () async {
                      DI.meetingDoctorsRepository.openVideoCall();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildFormInputFields() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: "User token",
      ),
      initialValue: _userToken,
      validator: (value) {
        if (value.isEmpty) {
          return "User token is mandatory";
        }
        return null;
      },
      onChanged: (value) {
        _userToken = value;
      },
    );
  }
}
