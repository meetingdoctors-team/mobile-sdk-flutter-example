import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';
import 'package:meeting_doctors_sdk_example/di/DI.dart';
import 'package:meeting_doctors_sdk_example/view/widgets/PermissionsCard.dart';
import 'package:meeting_doctors_sdk_example/view/widgets/UserSessionCard.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isLoading;
  bool _isInitialized;
  String _meetingDoctorsSdkUuid = "";

  @override
  void initState() {
    super.initState();
    _isInitialized = false;
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Meeting Doctors Demo'),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: EdgeInsets.all(16.0),
              child: ListView(
                physics: ClampingScrollPhysics(),
                children: [
                  if (!_isInitialized) welcomeWidget(),
                  if (_isInitialized) shutdownWidget(),
                  if (_isInitialized) homeWidget()
                ],
              ),
            ),
    );
  }

  Widget welcomeWidget() {
    return Column(
      children: [
        Text(
          "Welcome to Meeting Doctors demo. \n Press Start to initialize Meeting Doctors SDK",
          textAlign: TextAlign.center,
          style: TextStyle(height: 2),
        ),
        SizedBox(height: 16),
        RaisedButton(
          child: const Text('Start'),
          onPressed: () async {
            setState(() {
              _isLoading = true;
            });
            InitializeResult result = await DI.meetingDoctorsRepository.initialize(clientName: DI.clientName);
            setState(() {
              _isInitialized = result.success;
              _meetingDoctorsSdkUuid = result.uuid;
              _isLoading = false;
            });
          },
        ),
      ],
    );
  }

  Widget shutdownWidget() {
    return Column(
      children: [
        Text(
          "UUID: $_meetingDoctorsSdkUuid \n Press shutdown to clear session data",
          textAlign: TextAlign.center,
          style: TextStyle(height: 2),
        ),
        RaisedButton(
          child: const Text('Shutdown'),
          onPressed: () async {
            setState(() {
              _isLoading = true;
            });
            ShutdownResult result = await DI.meetingDoctorsRepository.shutdown();
            if (result.success) {
              setState(() {
                _isLoading = false;
                _isInitialized = false;
              });
            }
          },
        ),
      ],
    );
  }

  Widget homeWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PermissionsCard(),
        UserSessionCard(),
      ],
    );
  }
}
