//Original code: https://medium.com/@SebastianEngel/easy-push-notifications-with-flutter-and-firebase-cloud-messaging-d96084f5954f

import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io' show Platform;

class PushNotificationsManager {
  final FirebaseMessaging _firebaseMessaging;

  PushNotificationsManager(this._firebaseMessaging);

  Future<bool> requestPermission() async {
    if (Platform.isAndroid) {
      return true;
    }
    NotificationSettings notificationSettings = await _firebaseMessaging.requestPermission();
    if (notificationSettings.authorizationStatus == AuthorizationStatus.authorized) {
      return true;
    } else {
      return false;
    }
  }

  Future<String> getFcmToken() async {
    return await _firebaseMessaging.getToken();
  }
}
