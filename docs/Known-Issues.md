# Known issues in iOS
- Pending to add LICENSE file to plugin in case of being published
- It seems that the method 'Initialize' from native SDK is asynchronous, because sometimes UUID is not shown properly in Flutter
- In 'initialize' we should return a Flutter Error in all cases, to avoid possible issues in Flutter
```swift
let configuration = MediQuo.Configuration(id: clientName, secret: clientSecret, enableVideoCall: true, isDemo: true)
        let uuid: UUID? = MediQuo.initialize(with: configuration, options: launchOptions) {  resultInit in
            guard let value = resultInit.value else {
                NSLog("[SwiftMeetingDoctorsSdkPlugin] Installation failed: '\(String(describing: resultInit.error))'")
                //TODO ADD Flutter Error
                return;
            }
            NSLog("[SwiftMeetingDoctorsSdkPlugin] Mediquo framework initialization succeeded with identifier: '\(value.installationId)'")
        }
        let uuidAsString = uuid?.uuidString ?? "no uuid"
        NSLog("[SwiftMeetingDoctorsSdkPlugin] Synchronous installation identifier: '\(uuidAsString)'")
        result(uuidAsString)
```