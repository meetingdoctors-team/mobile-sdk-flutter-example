# Technical Summary
- This plugin needs development and manteinance, but at least we have validated that is possible integrate native Meeting Doctors SDK inside a Flutter application, but we have some limitations.
- In iOS 14, app in debug mode only can be launched if debugger is attached. So, if app is launched, and it is stopped automatically, probably this is the reason. More info [in this github issue](https://github.com/flutter/flutter/issues/60657)
- In this plugin we have used method channels as this was a straight forward solution (trigger events with parameters and launch native views, independent of Flutter Widgets).
- Another alternative could be platform views (wrap a native view inside Flutter Widget) but it seems that in the first version of this plugin, platform views didn't work properly in iOS.
- In case we need to receive some notifications and return a response to Flutter, probably we should use EventChannels.
In this post we can find documentation about different kinds of channels
https://stackoverflow.com/questions/56170451/what-is-the-difference-between-methodchannel-eventchannel-basicmessagechannel

##### Method Channels Summary

###### What went well
- By the moment, we haven't observed any issues with performance.
- Method Channels are stable and production ready, we don't have detected any issues or unexpected behavior

###### What can be improved
- Sometimes the integration can be really complex, because we can only use exchange simple types between platforms. Here we have the [types allowed](https://flutter.dev/docs/development/platform-integration/platform-channels#codec)
- Sometimes Meeting Doctors SDK receives native types that can't be send from Flutter. In this case, we need to investigate how to manage this case. For instance, we can't send from Flutter a `ÙIColor` but probably we could send a hexadecimal value for color, and then, in native side, build the `ÙIColor`
- Maintain the same look feel between Flutter app and Native app can be difficult to implement, difficult to maintain and very time consuming.

###### Recommendations
- Modify the native SDK to receive simple types instead of native objects, when it is possible. In this way, we can simplify the integration and take all benefits from Flutter. For instance, instead of receive a `ÙiColor`, native SDK could accept a String with the color in hexadecimal value, which is common between both platforms, Android and iOS.
- Investigate if there are a better way of pass data. By default, method channels use StandardCodec, but there are JsonCodec and BinaryCodec that could be investigated.
Here we have the implementation of codecs:
[Flutter Message Codecs](https://github.com/flutter/flutter/blob/master/packages/flutter/lib/src/services/message_codecs.dart)

##### Platform Views Summary
We haven't investigated the Platform Views alternative but we provide some links that could be useful in the future:
[Platform views official docs](https://flutter.dev/docs/development/platform-integration/platform-views)
[Wiki about Platform views](https://github.com/flutter/flutter/wiki/Android-Platform-Views)