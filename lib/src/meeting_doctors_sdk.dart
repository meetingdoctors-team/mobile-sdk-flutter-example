import 'dart:async';

import 'package:flutter/services.dart';

import 'types/types.dart';

class MeetingDoctorsSdk {
  static const MethodChannel _channel = const MethodChannel('meeting_doctors_sdk');

  MeetingDoctorsSdk();

  // Initialize Meeeting Doctors SDK
  //
  // Parameters:
  // [clientName] Client name that you should provide
  // [clientSecret] Api key provided by Meeting Doctors
  //
  // Returns InitializeResult:
  // [success]: true if platform method has been executed successfully
  // [uuid]: Installation identifier
  // [platformException]: PlatformException from native platform in case of error
  Future<InitializeResult> initialize({
    String clientName,
    String clientSecret,
  }) async {
    String uuid;
    try {
      uuid = await _channel.invokeMethod('initialize', {
        "clientName": clientName,
        "clientSecret": clientSecret,
      });
      return InitializeResult()
        ..success = true
        ..uuid = uuid;
    } on PlatformException catch (e) {
      uuid = "";
      print('PlatformException in initialize: ${e.message}');
      return InitializeResult()
        ..success = false
        ..platformException = e;
    }
  }

  // Authenticate with a user token
  //
  // Parameters:
  // [userToken] User token that you should provide
  //
  // Returns AuthenticateResult with properties:
  // [success]: true if platform method has been executed successfully
  // [isAuthenticated]: True if authentication is successful, false otherwise
  // [platformException]: PlatformException from native platform in case of error
  Future<AuthenticateResult> authenticate({String userToken}) async {
    bool isAuthenticated;
    try {
      isAuthenticated = await _channel.invokeMethod('authenticate', {
        "userToken": userToken,
      });
      return AuthenticateResult()
        ..success = true
        ..isAuthenticated = isAuthenticated;
    } on PlatformException catch (e) {
      isAuthenticated = false;
      print('PlatformException in authenticate: ${e.message}');
      return AuthenticateResult()
        ..success = false
        ..platformException = e;
    }
  }

  // To check if a user is authenticated or not
  //
  // Returns IsAuthenticatedResult with properties:
  // [success]: true if platform method has been executed successfully
  // [isAuthenticated]: True if authentication is successful, false otherwise
  // [platformException]: PlatformException from native platform in case of error
  Future<IsAuthenticatedResult> isAuthenticated() async {
    try {
      bool isAuthenticated = await _channel.invokeMethod('isAuthenticated');
      return IsAuthenticatedResult()
        ..success = true
        ..isAuthenticated = isAuthenticated;
    } on PlatformException catch (e) {
      print('PlatformException in isAuthenticated: ${e.message}');
      return IsAuthenticatedResult()
        ..success = false
        ..isAuthenticated = false
        ..platformException = e;
    }
  }

  // Send firebase token to meeting doctors sdk to register it
  //
  // Parameters:
  // [fcmToken] Firebase token that should be provided from Flutter side
  //
  // Returns SendFcmTokenResult with properties:
  // [success]: true if platform method has been executed successfully, so token is registered successfully
  // [platformException]: PlatformException from native platform in case of error
  Future<SendFcmTokenResult> sendFcmToken({String fcmToken}) async {
    if (fcmToken != null && fcmToken.isNotEmpty) {
      try {
        await _channel.invokeMethod('sendFcmToken', {
          "fcmToken": fcmToken,
        });
        return SendFcmTokenResult()..success = true;
      } on PlatformException catch (e) {
        print('PlatformException in sendFcmToken: ${e.message}');
        return SendFcmTokenResult()
          ..success = false
          ..platformException = e;
      }
    } else {
      return SendFcmTokenResult()..success = false;
    }
  }

  // Open chat native view.
  // In case of iOS, it will launch a view controller
  // In case of Android, it will launch Activity or Fragment
  //
  // Returns OpenChatResult with properties:
  // [success]: true if platform method has been executed successfully, so chat has been open
  // [platformException]: PlatformException from native platform in case of error
  Future<OpenChatResult> openChat() async {
    try {
      await _channel.invokeMethod('openChat');
      return OpenChatResult()..success = true;
    } on PlatformException catch (e) {
      print('PlatformException in openChat: ${e.message}');
      return OpenChatResult()
        ..success = false
        ..platformException = e;
    }
  }

  // Open video call native view.
  // In case of iOS, it will launch a view controller
  // In case of Android, it will launch Activity or Fragment
  //
  // Returns OpenChatResult with properties:
  // [success]: true if platform method has been executed successfully, so video call has been open
  // [platformException]: PlatformException from native platform in case of error
  Future<OpenVideoCallResult> openVideoCall() async {
    try {
      await _channel.invokeMethod('openVideoCall');
      return OpenVideoCallResult()..success = true;
    } on PlatformException catch (e) {
      print('PlatformException in openVideoCall: ${e.message}');
      return OpenVideoCallResult()
        ..success = false
        ..platformException = e;
    }
  }

  // To finish the user session.
  // Logout method delete all user cached data, including chat messages and professional list
  //
  // Returns LogoutResult with properties:
  // [success]: true if platform method has been executed successfully, so logout has been successful
  // [platformException]: PlatformException from native platform in case of error
  Future<LogoutResult> logout() async {
    try {
      await _channel.invokeMethod("logout");
      return LogoutResult()..success = true;
    } on PlatformException catch (e) {
      print('PlatformException in logout: ${e.message}');
      return LogoutResult()
        ..success = false
        ..platformException = e;
    }
  }

  // A shutdown method exists so a chat user can disconnect and erase all sensible information:
  //
  // Returns ShutdownResult with properties:
  // [success]: true if platform method has been executed successfully, so shutdown has been successful
  // [platformException]: PlatformException from native platform in case of error
  Future<ShutdownResult> shutdown() async {
    try {
      await _channel.invokeMethod("shutdown");
      return ShutdownResult()..success = true;
    } on PlatformException catch (e) {
      print('PlatformException in shutdown: ${e.message}');
      return ShutdownResult()
        ..success = false
        ..platformException = e;
    }
  }
}
