import 'package:flutter/services.dart';

abstract class SdkResult {
  bool success;
  PlatformException platformException;

  SdkResult({this.success, this.platformException});
}

class InitializeResult extends SdkResult {
  //UUID that identifies the installation identifier
  String uuid;
}

class AuthenticateResult extends SdkResult {
  //true if authentication is successful,
  //false if authentication has failed
  bool isAuthenticated;
}

class IsAuthenticatedResult extends SdkResult {
  //true if user is authenticated,
  //false if user is not authenticated
  bool isAuthenticated;
}

class SendFcmTokenResult extends SdkResult {}

class OpenChatResult extends SdkResult {}

class OpenVideoCallResult extends SdkResult {}

class LogoutResult extends SdkResult {}

class ShutdownResult extends SdkResult {}
