import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meeting_doctors_sdk/meeting_doctors_sdk.dart';

void main() {
  const MethodChannel channel = MethodChannel('meeting_doctors_sdk');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == "initialize") {
        return "1225BE43-48BD-4116-8487-1885A1CFAF67";
      } else if (methodCall.method == "authenticate") {
        return true;
      } else if (methodCall.method == "isAuthenticated") {
        return true;
      } else if (methodCall.method == "sendFcmToken") {
        return null;
      } else if (methodCall.method == "openChat") {
        return null;
      } else if (methodCall.method == "openVideoCall") {
        return null;
      } else if (methodCall.method == "logout") {
        return null;
      } else if (methodCall.method == "shutdown") {
        return null;
      } else {
        throw UnimplementedError("Not implemented");
      }
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('initialize should return installation identifier (UUID)', () async {
    InitializeResult initializeResult = await MeetingDoctorsSdk().initialize(
      clientName: "Meeting Doctors",
      clientSecret: "12345678",
    );

    expect(initializeResult.success, true);
    expect(initializeResult.uuid, "1225BE43-48BD-4116-8487-1885A1CFAF67");
  });

  test('authenticate should be successful', () async {
    AuthenticateResult authenticateResult = await MeetingDoctorsSdk().authenticate(
      userToken: "12345678",
    );

    expect(authenticateResult.success, true);
    expect(authenticateResult.isAuthenticated, true);
  });

  test('User is authenticated', () async {
    IsAuthenticatedResult isAuthenticatedResult = await MeetingDoctorsSdk().isAuthenticated();

    expect(isAuthenticatedResult.success, true);
    expect(isAuthenticatedResult.isAuthenticated, true);
  });

  test('Send firebase token should be successful', () async {
    SendFcmTokenResult sendFcmTokenResult = await MeetingDoctorsSdk().sendFcmToken(fcmToken: "12345678");

    expect(sendFcmTokenResult.success, true);
  });

  test('Open chat successfully', () async {
    OpenChatResult openChatResult = await MeetingDoctorsSdk().openChat();

    expect(openChatResult.success, true);
  });

  test('Open video call successfully', () async {
    OpenVideoCallResult openVideoCallResult = await MeetingDoctorsSdk().openVideoCall();

    expect(openVideoCallResult.success, true);
  });

  test('Logout successfully', () async {
    LogoutResult logoutResult = await MeetingDoctorsSdk().logout();

    expect(logoutResult.success, true);
  });

  test('Shutdown successfully', () async {
    ShutdownResult shutdownResult = await MeetingDoctorsSdk().shutdown();

    expect(shutdownResult.success, true);
  });
}
